<?php 

class Building{

    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){

        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

        // getter (accessor)
        public function getName(){
            return $this->name;
        }
    
        //setter (mutators)
        public function setName($name){
            $this->name = $name;  
        }
    
        public function getFloors(){
            return $this->floors;
        }
    
        private function setFloors($floors){
             $this->floors = $floors;
         }
    
         public function getAddress(){
            return $this->address;
        }
    
        private function setAddress($address){
             $this->address = $address;
         }
}



class Condominium extends Building{
 
    

}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City');


?>