<?php require_once('./code.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04 Access Modifiers and Encapsulation</title>
</head>
<body>
        <?php
			$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
		?>

    <h1>Building</h1>
    <p>The name of the building is <?php echo getName(); ?></p>
    <p>The <?php echo $building->name ?> has <?php echo $building->floors ?> floors</p>
    <p>The <?php echo $building->name ?> has <?php echo $building->address ?>.</p>
    <p>The name of the building has been changed to <?php $condominium->setName(''); ?></p>

    <h1>Condominium</h1>
    <p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
    <p>The <?php echo $condominium->getName->getName(); ?> has <?php //$condominium->getFloors(); ?>
    <p>The <?php echo $condominium->getName(); ?> is located at <?php $condominium->getAddress(); ?></p>
    <p>The name of the building has been changed to <?php // $condominium->setName('Enzo Tower')  ?></p>
</body>
</html>